#LazNewInclude package
The LazNewInclude package is an addon for Lazarus. It will register the filetype include so you can easily create a new include from File->New ...->Pascal Include and also inside packages. The package - as any other IDE addon - needs to be installed in Lazarus.
If you want a different default source for the include take a look at the CreateSource function of TFileDescPascalInclude.

#License
The code is based on TFileDescPascalInclude of the Lazarus Source. It is published under the GPL License.

Copyright (C) 2017: Johannes Rosleff Sörensen

This source is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

A copy of the GNU General Public License is available on the World Wide Web at <http://www.gnu.org/copyleft/gpl.html>. You can also
obtain it by writing to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1335, USA.﻿
