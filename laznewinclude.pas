{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit LazNewInclude;

{$warn 5023 off : no warning about unused units}
interface

uses
  NewInclude, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('NewInclude', @NewInclude.Register);
end;

initialization
  RegisterPackage('LazNewInclude', @Register);
end.
