{ The LazNewInclude package is an addon for Lazarus. It will register the filetype include so you can easily create a new include from
  File->New ...->Pascal Include and also inside packages. The package - as any other IDE addon - needs to be installed in Lazarus.
  If you want a different default source for the include take a look at the CreateSource function of TFileDescPascalInclude.
  The code is based on TFileDescPascalInclude of the Lazarus Source. It is published under the GPL License.

  Copyright (C) 2017: Johannes Rosleff Sörensen

  This source is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  A copy of the GNU General Public License is available on the World Wide Web at <http://www.gnu.org/copyleft/gpl.html>. You can also
  obtain it by writing to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1335, USA.
}


unit NewInclude;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, ProjectIntf,
  Forms, CompOptsIntf, Controls;

type
  { TFileDescPascalInclude }

  TFileDescPascalInclude = class( TProjectFileDescriptor )
    public
      constructor Create; override;
      function CreateSource( const aFilename, aSourceName,
                             aResourceName: string ): string; override;
      function GetLocalizedName: string; override;
      function GetLocalizedDescription: string; override;
      function CheckOwner( Quiet: boolean ): TModalResult; override;
  end;

procedure Register;


implementation

procedure Register;
begin
  RegisterProjectFileDescriptor( TFileDescPascalInclude.Create, FileDescGroupName );
end;

constructor TFileDescPascalInclude.Create;
begin
  inherited Create;
  Name:= 'Include';
  DefaultFilename:= 'include.inc';
  DefaultSourceName:= 'Include1';
  IsPascalUnit:= False;
end;

// The default source for the include file is suited to my needs. Please feel free to adapt the code.

function TFileDescPascalInclude.CreateSource( const aFilename, aSourceName,
  aResourceName: string ): string;
const
  LE = LineEnding;
begin
  Result:=
     '{ '+aFilename+' }'+LE
    +LE
    +'{$IFDEF INTERFACE}'+LE
    +LE
    +'{$ENDIF}'+LE
    +LE
    +'{$IFDEF IMPLEMENTATION}'+LE
    +LE
    +'{$ENDIF}'+LE
    +LE;
end;

function TFileDescPascalInclude.GetLocalizedName: string;
begin
  Result:= 'Pascal include';
end;

function TFileDescPascalInclude.GetLocalizedDescription: string;
begin
  Result:='Create a new pascal include.';
end;

// Source taken from TFileDescPascalUnit. Not sure if this is really required.
function TFileDescPascalInclude.CheckOwner( Quiet: boolean ): TModalResult;
begin
  Result:=inherited CheckOwner( Quiet );
  if ( Result <> mrOK ) then exit;
  if ( Owner = nil ) then exit;
  if ( Assigned( CheckCompOptsAndMainSrcForNewUnitEvent )) then
    if ( Owner is TLazProject ) then
      Result:= CheckCompOptsAndMainSrcForNewUnitEvent( Owner.LazCompilerOptions );
end;

end.

